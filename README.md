# Inkscape Glitch Effect Extension

## Parameters

- spacing
- lines
- width
- skew

## Resources:

- [Inkscape Extensions Repository](https://gitlab.com/inkscape/extensions)
- [Inkscape Developing Extensions Guide](https://inkscape.org/develop/extensions)
- [Inkscape Glitch Effect Tutorial](https://youtu.be/ymI1uY_-kNM)

#!/usr/env/python
# coding: utf-8
# SPDX-License-Identifier: GPL-3.0-or-later

"""
This extension creates a glitch effect.
"""

import inkex

class Glitch(inkex.EffectExtension):
    pass

if __name__ == '__main__':
    Glitch().run()
